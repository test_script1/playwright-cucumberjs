# Recustom QA Challenge

In order to complete the challenge, you will need to do the following

- Write a test case for the below user story
- Commit the manual test cases to this repo
- Share this repo with saalihou@gmail.com

### Optional (but appreciated in order to show your test automation experience)

- Automate the test case using your preferred web automation tool
- Commit the automated test case script in this repo

# User Story - Search for Haggadah and Share

As a user I want to be able to search and share my favorite haggadahs so I can spread the word around jewish rituals

**Acceptance Criteria**

- On haggadot.com, I can search for a Haggadah next to my favorite haggadahs using a keyword
- When I do so, a list of haggadahs matching the keyword are presented
- I can share a specific haggadah from the results
- When I share the haggadah, a link is presentend to me that I can copy for sharing purposes
- I can also share the haggadah as a post on Facebook, Twitter or LinkedIn

## Installation

### Prerequisites

- Node 14 or newer: `node -v`

```bash
npm install
npx playwright install
npx playwright install-deps --dry-run
```

## To run your tests

# Ubuntu / macOS (Headless Mode)

```bash
ENV=production npx cucumber-js
```

# Windows (Headless Mode)

```bash
$env:ENV = "production"; npx cucumber-js
```

# Ubuntu / macOS (GUI Mode)

```bash
ENV=production HEADLESS=false npx cucumber-js
```

# Windows (GUI Mode)

```bash
$env:ENV = "production"; $env:HEADLESS = "false";npx cucumber-js
```

### Environment variables

| Variable     |                                                     |
| ------------ | --------------------------------------------------- |
| ENV          | base URL is sourced from the '/config/env.{}' file. |
| HEADLESS     | "false" to open the browser. Default: `false`.      |
| --parallel=3 | Appends this tag to execute tests in parallel.      |

### Local reports

If you want to see the reports after running tests, open the newly generated `report/{ENV}.html` file.

If your test has failed, you should also see a screenshot attached to the failed scenario and also video recording
(For demo purpose I am capturing screenshots and video in either of cases)

### Playwright

`./hooks/world.ts` - to configure the device, browser, headless mode or other playwright runner option.

### VSCode

Download **Cucumber (Gherkin) Full Support** extension and configure paths.

## Guidelines

- See [Gherkin Reference](https://cucumber.io/docs/gherkin/reference/) on
  how and when to use `Given`, `When`, `Then` and other special keywords for the feature files.
- See these [tips for POM classes](https://angiejones.tech/page-object-model/) or [this summary](pages/pom.md) with typescript examples.

## Official documentation

- Cucumber: https://cucumber.io/
- Playwright: https://playwright.dev/docs/intro
